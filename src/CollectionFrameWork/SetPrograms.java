package CollectionFrameWork;

import java.util.Iterator;
import java.util.TreeSet;

public class SetPrograms {

	public static void main(String[] args) {
		/*
		 * HashSet<String> hs = new HashSet();
		 * 
		 * hs.add("Rounak"); hs.add("Rounak"); hs.add("vikash"); hs.add(null);
		 * 
		 * 
		 * Iterator<String> itr = hs.iterator();
		 * 
		 * while (itr.hasNext()) { System.out.println(itr.next()); }
		 */
		/*
		 * LinkedHashSet<String> hs = new LinkedHashSet(); hs.add("Rounak");
		 * hs.add("Rounak"); hs.add("vikash"); hs.add(null); Iterator<String> itr =
		 * hs.iterator(); while (itr.hasNext()) { System.out.println(itr.next()); }
		 * 
		 * System.out.println(itr.hasNext()); }
		 */

		TreeSet<String> hs = new TreeSet();
		hs.add("Aditi");
		hs.add("Rounak");
		hs.add("Julee");
		hs.add("Rounak");
		hs.add("vikash");

		System.out.println(hs.last());

		Iterator<String> itr = hs.iterator();
		while (itr.hasNext()) {
			System.out.println(itr.next());
		}

	}

}
