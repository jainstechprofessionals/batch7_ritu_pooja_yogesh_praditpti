package CollectionFrameWork;

import java.util.LinkedList;

public class LinkedListExample {

	public static void main(String[] args) {
		LinkedList<String> lst = new LinkedList();
		lst.add("value1");
		lst.add("Value2");
		lst.add("Value3");
		lst.add(1, "Added Value");
		lst.remove(2);
		for (int i = 0; i < lst.size(); i++) {
			System.out.println(lst.get(i));
		}
		System.out.println(lst.contains("value1"));		
		LinkedList<String> lst1 = new LinkedList();
		lst1.addAll(lst);
		lst1.add("asdfsadf");
		System.out.println(lst1);		
		LinkedList<String> lst2 = (LinkedList<String>) lst1.clone();
		System.out.println(lst);
	}
}
