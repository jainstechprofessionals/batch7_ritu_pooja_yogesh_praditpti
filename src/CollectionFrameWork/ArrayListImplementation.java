package CollectionFrameWork;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

public class ArrayListImplementation {

	public static void main(String[] args) {

		ArrayList<String> lst = new ArrayList();
		lst.add("value1");
		lst.add("value1");
		lst.add("Value2");
		lst.add("Value3");

		lst.add(1, "Added Value");
	

		for (int i = 0; i < lst.size(); i++) {
			System.out.println(lst.get(i));
		}
		System.out.println(lst.contains("value1"));
		

		/*
		 * for(String str : lst) { System.out.println(str);
		 * 
		 * }
		 */
		/*
		 * Iterator itr = lst.iterator(); while(itr.hasNext()) {
		 * System.out.println(itr.next()); }
		 */

	}
}
