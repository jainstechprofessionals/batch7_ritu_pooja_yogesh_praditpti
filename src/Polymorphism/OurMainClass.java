package Polymorphism;

public class OurMainClass {

	public static void main(String[] args) {
		
		
		//Vehicle vk1 =new Vehicle();// Static binding   (Early binding)
		
		Vehicle vk =new Bike();  //upcasting   // Dynamic Binding   (Late Binding)
		double fa;
		Bank bnk;
		
		 bnk = new SBI();
		 fa= bnk.rateOfInterest();
		System.out.println(fa);
		

		 bnk = new ICICI();
		 fa = bnk.rateOfInterest();
		System.out.println(fa);
		
		
		//single
		// multilevel
		// he
	
		
		
	}
	
	
	
	
	
}
