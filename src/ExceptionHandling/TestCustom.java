package ExceptionHandling;

public class TestCustom {

	public void checkAge(int age) throws CustomException {
		if(age<18) {
			throw new CustomException("not valid");
		}
	}
	
	public static void main(String[] args) throws CustomException {
		
		TestCustom obj = new TestCustom();
		obj.checkAge(15);
	}

}
