package ExceptionHandling;

public class ExpHandlePrgm {

	public void show() {
		try {
			Thread.sleep(10000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	public void display() {
		try {
			int a = 10000;
			int b = 6;
			System.out.println(a / b);
		} catch (Exception obj) {

		}
	}

	public void show1() {
		int age = 9;
		if (age < 18) {
			throw new ArithmeticException("not valid");
		}
	}

	public static void main(String[] args) {

		ExpHandlePrgm obj1 = new ExpHandlePrgm();
		obj1.show1();

		try {
			int a = 10000;
			int b = 6;
			System.out.println(a / b);
			String s = null;
			System.out.println(s.length());
			
			int arr[] = new int[5];
			arr[15] = 80;

		}  

		catch (ArithmeticException obj) {
			System.out.println("Neumrator should be greater then zero");
		} catch (NullPointerException obj) {
			obj.printStackTrace();
			System.out.println("Please enter valid string");
		}
		catch (Exception obj) {
			obj.printStackTrace();
			System.out.print("other exception");
		}
		finally {
			System.out.println("sadfsadf");
		}

	}

	// try
	// catch
	// finally
	// throw
	// throws
}
