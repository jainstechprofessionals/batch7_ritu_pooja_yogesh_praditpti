
public class OuterClass {

	public void display() {
		System.out.print("in display");
	}

	public class innerWithoutStatic {

		public void show() {
			display();
		}
	}

	public static class InnerStatic {

		public void sum() {
			System.out.print("in shum");
		}
	}

	public static void main(String[] ar) {
		OuterClass outer = new OuterClass();
		OuterClass.innerWithoutStatic innerWitout = 
				
				outer.new innerWithoutStatic();
		
		OuterClass.InnerStatic  innerStatic = 
				new OuterClass.InnerStatic();
		
		innerWitout.show();
	}

}
