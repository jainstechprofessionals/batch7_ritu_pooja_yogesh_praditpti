package Inhertance;

public class ThisAsAnArguments {
   int a;
   int b;
	public ThisAsAnArguments show() {
		this.a=10;
		System.out.println("Show");
		return this;
	}
	
	public ThisAsAnArguments display() {
		this.b=20;
		System.out.println("display");
		return this;
	}
	
	public void view() {
		System.out.println(this.a+this.b);
	}
	
	public static void main(String[] args) {
		ThisAsAnArguments obj = new ThisAsAnArguments();
         obj.show().display().view();
       

         
         
}}
