package Inhertance;

public class ProgramKeyword {

	int a = 10; // instance
	int bonus;
	String name;

	public ProgramKeyword() {
	
		this.bonus = 10;
		
	}

	public ProgramKeyword(int sal) {
	
		int totalSal = sal + this.bonus;
		System.out.print(totalSal);

	}

	public void show() {

		a = 10;

	}

	public void display() {

		int a;
		a = 30;

	}

	public void view(int a) {
		this.a = a;
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		ProgramKeyword obj = new ProgramKeyword(1000);

	}

}
